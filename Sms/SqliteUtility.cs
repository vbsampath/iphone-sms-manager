﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Finisar.SQLite;

namespace SmsiPhone
{
    class SqliteUtility
    {

        private SQLiteConnection sql_con = null;
        private SQLiteCommand sql_cmd = new SQLiteCommand();
        private SQLiteDataAdapter DB = null;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        Connections connections = new Connections();

        public SqliteUtility()
        {
            if(sql_con == null)
            {
                try
                {
                    this.sql_con = new SQLiteConnection(connections.getSmsDatabaseConnectionString());
                    this.sql_con.Open();
                }
                catch (SQLiteException ex)
                {
                    
                    throw ex;
                }
            }
        }

        ~SqliteUtility()
        {
            try
            {
                this.sql_con.Close();
            }
            catch (SQLiteException ex)
            {
                throw ex;
            }
        }

        public DataSet ExecuteDataAdapter(string query)
        {
            DataSet dataSet = new DataSet();

            try
            {
                this.DB = new SQLiteDataAdapter(query, this.sql_con);
                this.DB.Fill(dataSet);
            }
            catch (SQLiteException sqe)
            {
                Console.WriteLine(sqe.Message);
                throw sqe;
            }

            return dataSet;
        }

        public int ExecuteNonQuery(string query)
        {
            int result;
            try
            {
                this.sql_cmd.CommandText = query;
                this.sql_cmd.Connection = this.sql_con;
                result = this.sql_cmd.ExecuteNonQuery();
            }
            catch (SQLiteException sqe)
            {
                Console.WriteLine(sqe.Message);
                throw sqe;
            }
            
            return result;
        }

        public int ExecuteScalar(string query)
        {
            Object result;
            int primaryKey=0;
            Object last_insert_rowidObject;

            try
            {
                this.sql_cmd.CommandText = query;
                this.sql_cmd.Connection = this.sql_con;
                SQLiteCommand last_insert_rowid = new SQLiteCommand("SELECT last_insert_rowid()", this.sql_con);
                
                this.sql_con.BeginTransaction();
                result = this.sql_cmd.ExecuteScalar();
                last_insert_rowidObject = last_insert_rowid.ExecuteScalar();
                primaryKey = Convert.ToInt16(last_insert_rowidObject);
                this.sql_cmd.Transaction.Commit();
            }
            catch (SQLiteException sqe)
            {
                Console.WriteLine(sqe.Message);
                throw sqe;
            }

            return primaryKey;
        }

        public string escapeQuotes(string text)
        {
            text = text.Replace("\'", "''");
            text = text.Replace("\"", "\"\"");
            return text;
        }
    }
}
