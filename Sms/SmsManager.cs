﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Globalization;
using System.Data;
using System.Text.RegularExpressions;

namespace SmsiPhone
{
    class SmsManager
    {
        SqliteUtility sqliteUtility = new SqliteUtility();
        Connections connections = new Connections();
        SmsExportObject smsExportObject = new SmsExportObject();
        Dictionary<string, Int64> addresses = null;
        Int64 group_id = 0;

        public SmsManager()
        {
            GetAddresses();
        }

        public void GetAddresses()
        {
            this.addresses = this.GetMessageGroups();
        }

        public bool CheckGroupMemberExists(string address)
        { 
            bool isExists = false;
            int isCreated = 0;

            //generate key because it doesnt exists
            if (!addresses.ContainsKey(address))
            {
                if (MainWindow.isDryRun)
                {
                    //insert into msg_group to get group_id for next opetations
                    string msg_group_query = String.Format(@"INSERT INTO {0} ({1}, {2}, {3}, {4}) VALUES({5},{6},{7},{8})",
                        Constants.Static_MappingsMessageGroup, "type", "newest_message", "unread_count", "hash", "0", "0", "0", "0");
                    string query = String.Format(@"INSERT INTO {0} ({1}, {2}, {3}) VALUES({4},""{5}"",""{6}"")",
                        Constants.Static_MappingsGroupstable, "group_id", "address", "country", this.group_id, address, "in");
                    isExists = true;
                    this.addresses.Add(address,this.group_id);
                }
                else
                {
                    //insert into msg_group to get group_id for next opetations
                    string msg_group_query = String.Format(@"INSERT INTO {0} ({1}, {2}, {3}, {4}) VALUES({5},{6},{7},{8})",
                        Constants.Static_MappingsMessageGroup, "type", "newest_message", "unread_count", "hash", "0", "0", "0", "0");
                    this.group_id = sqliteUtility.ExecuteScalar(msg_group_query);
                    if (this.group_id > 0)
                    {
                        //adding newest element to list such that future fetches will be on the list and not on database
                        this.addresses.Add(address, this.group_id);

                        string query = String.Format(@"INSERT INTO {0} ({1}, {2}, {3}) VALUES({4},""{5}"",""{6}"")",
                        Constants.Static_MappingsGroupstable, "group_id", "address", "country", this.group_id, address, "in");
                        isCreated = sqliteUtility.ExecuteNonQuery(query);
                        if (isCreated > 0)
                        {
                            isExists = true;    
                        }
                    }
                }
                
            }
            else
            {
                isExists = true;
                
                //set group_id for this iteration
                this.group_id = this.GetGroupIdAddress(address);
            }
            
            return isExists;
        }
        public Int64 GetGroupIdAddress(string address)
        {
            Int64 groupId = 0;
            groupId = this.addresses[address];
            return groupId;
        }

        public string GetSmsInsertQuery(string address, string body, int type, int date, string smsObjectType)
        {
            string query = "";
            int UIFlags = 0;
            
            UIFlags = this.ApplyUIFlags(body);
            type = this.ApplyFlags(type, smsObjectType);

            query = String.Format(@"INSERT INTO {0} ({1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18}) VALUES(""{19}"",{20},""{21}"",{22},{23},{24},""{25}"",{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36})",
                Constants.Static_MappingsSmstable
                , "address"
                , "date"
                , "text"
                , "flags"
                , "group_id"
                , "UIFlags"
                , "country"
                , "replace"
                , "version"
                , "read"
                , "association_id"
                , "height"
                , "madrid_version"
                , "madrid_type"
                , "madrid_error"
                , "is_madrid"
                , "madrid_date_read"
                , "madrid_date_delivered"
                , String.Format("{0}", address)
                , date
                , String.Format("{0}", sqliteUtility.escapeQuotes(body))
                , type
                , this.group_id
                , UIFlags
                , String.Format(@"{0}", "in")
                , 0
                , 0
                , 1
                , 0
                , 0
                , 0
                , 0
                , 0
                , 0
                , 0
                , 0
                );
            return query;
        }

        public bool createSms(Object smsObject)
        {
            bool isCreated = false;
            int result = 0;
            string query = null;
            if (smsObject.GetType().ToString() == "SmsiPhone.GoSms")
            {
                GoSms sms = (GoSms) smsObject;
                query = this.GetSmsInsertQuery(sms.address, sms.body, sms.type, sms.date, "GoSms");
            }
            else if (smsObject.GetType().ToString() == "SmsiPhone.LocalSms")
            {
                LocalSms sms = (LocalSms)smsObject;
                query = this.GetSmsInsertQuery(sms.address, sms.body, sms.type, sms.date, "Sms");
            }

            if (MainWindow.isDryRun)
            {
                result = 1; //its just dry run so all will insert
            }
            else
            {
                result = sqliteUtility.ExecuteNonQuery(query);
            }
            
            if (result > 0)
            {
                isCreated = true;
            }
            return isCreated;
        }

        public int ApplyUIFlags(string smsBody)
        {
            int UIFlags = 4; //default mode. not having any URLs or phone numbers
            bool urlPresent = false;
            bool phoneNumberPresent = false;
            Regex regx = new Regex(Constants.Static_RegexPatternsUrlPattern, RegexOptions.IgnoreCase);
            urlPresent = regx.IsMatch(smsBody);
            Regex rx = new Regex(Constants.Static_RegexPatternsPhonePattern, RegexOptions.IgnoreCase);
            phoneNumberPresent = rx.IsMatch(smsBody);

            if(urlPresent || phoneNumberPresent)
            {
                UIFlags = 5;
            }
           
            return UIFlags;
        }

        public int ApplyFlags(int smsType, string smsObjectType)
        {
            int flags = 0;

            if (smsType == 1)
            {
                flags = 2;
            }
            else if (smsType == 2)
            {
                flags = 3;
            }
            
            return flags;
        }

        public bool PrepareDatabase()
        {
            bool isPrepared = false;
            //get queries list
            List<string> queries = new List<string>();
            queries = Constants.Static_QueriesRemoveConflicts;

            //remove conflicting triggers
            if (MultipleQueryOperations(queries))
            {
                isPrepared = true;
            }
            return isPrepared;
        }

        public bool FinalizeDatabase()
        {
            bool isFinalized = false;
            //get queries list
            List<string> queries = new List<string>();
            queries = Constants.Static_QueriesRestoreConflicts;

            //restore conflicting triggers
            if (MultipleQueryOperations(queries))
            {
                isFinalized = true;
            }
            return isFinalized;
        }

        public bool MultipleQueryOperations(List<string> queries)
        { 
            bool isExecuted;
            int queryCount = queries.Count;
            int result=0;

            for (int i = 0; i < queryCount; i++)
            {
                result = sqliteUtility.ExecuteNonQuery(queries.ElementAt(i).ToString());
                if (result == 0)   //TODO This is a bug. Differs for DROP, CREATE and INSERT, UPDATE (DDL and DML)                  
                {
                    result = 1;
                }
                else
                {
                    result = 0;
                }
            }

            if (result > 0)
            {
                isExecuted = true;
            }
            else
            {
                isExecuted = false;
            }

            return isExecuted;
        }

        public Dictionary<string, Int64> GetMessageGroups()
        {
            string smsGroupsSQLiteQuery = Constants.Static_SmsGroupsQuery;
            Dictionary<string, Int64> result = null;
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();

            try
            {
                dataSet = sqliteUtility.ExecuteDataAdapter(smsGroupsSQLiteQuery);
                dataTable = dataSet.Tables[0];
                result = dataTable.Rows.OfType<DataRow>().
                                    ToDictionary(row => row.Field<string>("address"), d => d.Field<Int64>("group_id"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }

            return result;
        }

        #region parsing xml
        public string ParseSmsXml()
        {
            XDocument xdoc = new XDocument();

            //go sms
            xdoc = XDocument.Load(connections.getGoSmsXmlConnectionString());
            var items = from item in xdoc.Descendants("GoSms").Elements("SMS").Skip(1).OrderBy(x => Convert.ToInt64(x.Element("date").Value), new SmsiPhone.MainWindow.IntComparer())
                        select item.PreviousNode;
            this.smsExportObject.goSms = new List<GoSms>();
            foreach (XElement item in items)
            {
                GoSms goSms = new GoSms(item);
                this.smsExportObject.goSms.Add(goSms);
            }

            //local sms
            xdoc = XDocument.Load(connections.getSmsXmlConnectionString());

            //Parsing the localstore Xml
            IEnumerable<XElement> smss = xdoc.Descendants("localsmsstore").Descendants("sms");

            this.smsExportObject.localSms = new List<LocalSms>();

            foreach (XElement item in smss)
            {
                LocalSms sms = new LocalSms(item);
                this.smsExportObject.localSms.Add(sms);
            }
            
            return String.Format("Processed GoSms {0} and LocalSms {1}. Total Sms Processed are {2}", this.smsExportObject.goSms.Count, this.smsExportObject.localSms.Count, this.smsExportObject.goSms.Count + this.smsExportObject.localSms.Count);
        }
        #endregion

        #region Insert Sms into database
        public string SaveSms()
        {
            int gosmsInsertCount = 0;
            int localsmsInsertCount = 0;
            foreach (var goSmsItem in this.smsExportObject.goSms)
            {
                //if group_member doesnt exists then create new ones
                if (this.CheckGroupMemberExists(goSmsItem.address))
                {
                    //create sql statement for inserting
                    bool result = this.createSms(goSmsItem);
                    if (result) gosmsInsertCount++;
                }
            }

            //insert localstore sms
            foreach (var smsItem in this.smsExportObject.localSms)
            {
                //if group_member doesnt exists then create new ones
                if (this.CheckGroupMemberExists(smsItem.address))
                {
                    //create sql statement for inserting
                    bool result = this.createSms(smsItem);
                    if (result) localsmsInsertCount++;
                }
            }

            return String.Format("Inserted GoSms {0} and LocalSms {1}. Total Sms Inserted are {2}", gosmsInsertCount, localsmsInsertCount, gosmsInsertCount + localsmsInsertCount);
        }
        #endregion
    }

    class SmsExportObject
    { 
        public List<GoSms> goSms {get; set;}
        public List<LocalSms> localSms {get; set;}
    }

    /// <summary>
    /// A GoSms object represents a single GoSms sms from xml
    /// </summary>
    class GoSms
    {
        public string id { get; set; }
        public string address { get; set; }
        public int date { get; set; }
        public int read { get; set; }
        public string status { get; set; }
        public int type { get; set; }
        public string body { get; set; }
        public int locked { get; set; }

        private static string GetElementValue(XContainer element, string name)
        {
            if ((element == null) || (element.Element(name) == null))
                return String.Empty;
            return element.Element(name).Value;
        }

        public GoSms(XElement item)
        {
            // Get the string properties from the GoSms's element values
            id = item.Element("_id").Value;
            address = item.Element("address").Value;
            date = Convert.ToInt32(item.Element("date").Value.Substring(0,10));
            read = Convert.ToInt16(item.Element("read").Value);
            status = item.Element("status").Value;
            type = Convert.ToInt16(item.Element("type").Value);
            body = item.Element("body").Value;
            locked = Convert.ToInt16(item.Element("locked").Value);
        }

        public GoSms()
        {
        }
    }

    /// <summary>
    /// A LocalSms object represents a single Sms sms from xml
    /// </summary>
    class LocalSms
    {
        public string address { get; set; }
        public string recipient { get; set; }
        public int date { get; set; }
        public int type { get; set; }
        public string body { get; set; }

        private static string GetElementValue(XContainer element, string name)
        {
            if ((element == null) || (element.Element(name) == null))
                return String.Empty;
            return element.Element(name).Value;
        }

        public LocalSms(XElement item)
        {
            // Get the string properties from the GoSms's element values
            address = item.Element("number").Value;
            recipient = item.Element("recipient").Value;
            type = 2; // 2 for sent and 1 for recieved
            DateTime result;
            string format = "dd-MM-yyyy HH:mm:ss";
           
            if (DateTime.TryParseExact(item.Element("sent").Value, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                TimeSpan ts = new TimeSpan(5, 30, 0);
                DateTimeOffset dateTimeOffset = new DateTimeOffset(result, ts);
                DateTime temp = new DateTime();
                temp = dateTimeOffset.DateTime;
                date = DateTimeToUnixTimestamp(temp);
            }
            body = item.Element("body").Value;
        }

        public LocalSms()
        {
        }

        public static int DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return Convert.ToInt32((dateTime - new DateTime(1970, 1, 1, 0, 0, 0).ToLocalTime()).TotalSeconds);
        }
    }
}
