﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Xml;

namespace SmsiPhone
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<GoSms> goSmsList { get; set; }
        private List<LocalSms> smsList { get; set; }
        SmsManager smsManager = new SmsManager();
        Connections connections = new Connections();
        public static bool isDryRun {get; set;}

        public static bool getIsDryRun()
        {
            return isDryRun;
        }

        public MainWindow()
        {
            InitializeComponent();

            //setting dry run more
            isDryRun = false;
        }
        
        private void btnPrepareDatabase_Click(object sender, RoutedEventArgs e)
        {
            //prepare database. Remove conflicting triggers and update with new ones
            if (!isDryRun)
            {
                this.smsManager.PrepareDatabase();
                this.statusTextBox.Text = "Database Prepared\r\n";
                this.statusTextBox.TextWrapping = TextWrapping.WrapWithOverflow;
                this.statusTextBox.AppendText("=============================================\r\n");
                this.statusTextBox.AppendText("Below are the queries executed for preparing database\r\n");
                foreach (string query in Constants.Static_QueriesRemoveConflicts)
                {
                    this.statusTextBox.AppendText(query.ToString() + "\r\n");
                }
                this.statusTextBox.AppendText("=============================================\r\n");
            }
        }

        private void btnGetSms_Click(object sender, RoutedEventArgs e)
        {
            string result = smsManager.ParseSmsXml();
            this.statusTextBox.AppendText("Parsing Sms Xml's\r\n");
            this.statusTextBox.AppendText("=============================================\r\n");
            this.statusTextBox.AppendText(result + "\r\n");
            this.statusTextBox.AppendText("=============================================\r\n");
        }

        private void btnSaveSms_Click(object sender, RoutedEventArgs e)
        {
            string result = smsManager.SaveSms();
            this.statusTextBox.AppendText("Saving Sms's\r\n");
            this.statusTextBox.AppendText("=============================================\r\n");
            this.statusTextBox.AppendText(result + "\r\n");
            this.statusTextBox.AppendText("=============================================\r\n");
        }

        private void btnFinalizeDatabase_Click(object sender, RoutedEventArgs e)
        {
            /*
             * finalize the database.
             * Restore Triggers
             * Update _SqliteDatabaseProperties Table with updated values
             * */
            if (!isDryRun)
            {
                this.smsManager.FinalizeDatabase();
                this.statusTextBox.AppendText("Finalized Database\r\n");
                this.statusTextBox.AppendText("=============================================\r\n");
                this.statusTextBox.AppendText("Below are the queries executed for finalizing database\r\n");
                foreach (string query in Constants.Static_QueriesRestoreConflicts)
                {
                    this.statusTextBox.AppendText(query.ToString() + "\r\n");
                }
                this.statusTextBox.AppendText("=============================================\r\n");
                this.statusTextBox.AppendText("Completed\r\n");
            }
        }


        public class IntComparer : IComparer<long>
        {
            public int Compare(long x, long y)
            {
                if (x > y)
                    return 1;
                if (x < y)
                    return -1;
                else
                    return 0;
            }
        }
    }
}
