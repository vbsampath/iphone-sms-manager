﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SmsiPhone
{
    public class Constants
    {
        #region app settings
        public static string Static_SmsDatabasePath
        {
            get
            {
                return ConfigurationManager.AppSettings["smsDatabasePath"];
            }
        }
        public static string Static_GoSmsPath
        {
            get
            {
                return ConfigurationManager.AppSettings["goSmsPath"];
            }
        }
        public static string Static_SmsPath
        {
            get
            {
                return ConfigurationManager.AppSettings["smsPath"];
            }
        }
        public static string Static_SmsGroupsQuery
        {
            get
            {
                return ConfigurationManager.AppSettings["smsGroupsQuery"];
            }
        }
        #endregion

        #region Mappings
        public static string Static_MappingsSmstable
        {
            get
            {
                return Mappings.Settings.smstable;
            }
        }
        public static string Static_MappingsGroupstable
        {
            get
            {
                return Mappings.Settings.groupstable;
            }
        }
        public static string Static_MappingsMessageGroup
        {
            get
            {
                return Mappings.Settings.messagegroup;
            }
        }
        #endregion

        #region Queries
        public static List<string> Static_QueriesRemoveConflicts
        {
            get
            {
                string[] lines = null;
                lines = Regex.Split(Queries.Settings.removeconflicts, "\r\n");
                return lines.ToList();
            }
        }
        public static List<string> Static_QueriesRestoreConflicts
        {
            get
            {
                string[] lines = null;
                lines = Regex.Split(Queries.Settings.restoreconflicts, "\r\n");
                return lines.ToList();
            }
        }
        #endregion

        #region RegexPatterns
        public static string Static_RegexPatternsPhonePattern
        {
            get
            {
                return RegexPatterns.Settings.phonePattern;
            }
        }
        public static string Static_RegexPatternsUrlPattern
        {
            get
            {
                return RegexPatterns.Settings.urlPattern;
            }
        }
        #endregion

    }


    //Mappings section class for Tables Mappings
    public class Mappings : ConfigurationSection
    {
        private static Mappings settings = ConfigurationManager.GetSection("mappings") as Mappings;

        public static Mappings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("smstable", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength=1, MaxLength=256)]
        public string smstable
        {
            get { return (string)this["smstable"]; }
            set { this["smstable"] = value; }
        }

        [ConfigurationProperty("groupstable", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength = 1, MaxLength = 256)]
        public string groupstable
        {
            get { return (string)this["groupstable"]; }
            set { this["groupstable"] = value; }
        }

        [ConfigurationProperty("messagegroup", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength = 1, MaxLength = 256)]
        public string messagegroup
        {
            get { return (string)this["messagegroup"]; }
            set { this["messagegroup"] = value; }
        }
    }

    //Queries section class for queries
    public class Queries : ConfigurationSection
    {
        private static Queries settings = ConfigurationManager.GetSection("queries") as Queries;

        public static Queries Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("removeconflicts", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength=1, MaxLength=256)]
        public string removeconflicts
        {
            get { return (string)this["removeconflicts"]; }
            set { this["removeconflicts"] = value; }
        }

        [ConfigurationProperty("restoreconflicts", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength=1, MaxLength=256)]
        public string restoreconflicts
        {
            get { return (string)this["restoreconflicts"]; }
            set { this["restoreconflicts"] = value; }
        }
    }

    //RegexPatterns section class for regex patterns
    public class RegexPatterns : ConfigurationSection
    {
        private static RegexPatterns settings = ConfigurationManager.GetSection("regexpatterns") as RegexPatterns;

        public static RegexPatterns Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("phonePattern", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength=1, MaxLength=256)]
        public string phonePattern
        {
            get { return (string)this["phonePattern"]; }
            set { this["phonePattern"] = value; }
        }

        [ConfigurationProperty("urlPattern", IsRequired = false)]
        //[StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;’\"|\\", MinLength=1, MaxLength=256)]
        public string urlPattern
        {
            get { return (string)this["urlPattern"]; }
            set { this["urlPattern"] = value; }
        }
    }

    public class Connections
    {
        public string getGoSmsXmlConnectionString()
        {
            return Constants.Static_GoSmsPath;
        }

        public string getSmsXmlConnectionString()
        {
            return Constants.Static_SmsPath;
        }
        public string getSmsDatabaseConnectionString()
        {
            string smsPath = Constants.Static_SmsDatabasePath;
            string connectionString = "Data Source=" + smsPath + ";Version=3;New=False;Compress=True;";
            return connectionString;
        }
    }
}
